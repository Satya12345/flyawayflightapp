import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
   private baseUri='https://reqres.in/api'
  constructor(private http:HttpClient, private router:Router) { }
  private registerUri = `${this.baseUri}/register`;
  private loginUri=`${this.baseUri}/login`;

  userRegistration(body){
    return this.http.post(this.registerUri,body);

    //http method return observables
    
  }

  userLogin(body){
    

    //http method return observables

    return this.http.post(this.loginUri,body);
    
  }
  isuserLoggedIn(){
    if(sessionStorage.getItem('token')){

      return true;

    }else{
      return false;
    }
  }
logOut(){
  sessionStorage.clear();
  sessionStorage.removeItem('token');
  this.router.navigateByUrl('auth/signin');
}

}
