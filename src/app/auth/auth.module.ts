import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { Routes , RouterModule } from '@angular/router';
import { FormsModule } from'@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { AuthService } from './auth.service';


const routes: Routes = [
  {path:"signin" , component:LoginComponent},
  {path:"signup" , component:RegisterComponent}
];

@NgModule({
  declarations: [
    LoginComponent , RegisterComponent 
  ],
  imports: [

     FormsModule, HttpClientModule , RouterModule.forChild(routes)
  ],
  providers:[AuthService],
  exports: [RouterModule]
})
export class AuthModule { }
